<?php

namespace App\Repository;

use App\Entity\Pokemon;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Pokemon|null find($id, $lockMode = null, $lockVersion = null)
 * @method Pokemon|null findOneBy(array $criteria, array $orderBy = null)
 * @method Pokemon[]    findAll()
 * @method Pokemon[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PokemonRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Pokemon::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(Pokemon $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(Pokemon $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    // /**
    //  * @return Pokemon[] Returns an array of Pokemon objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    
    public function findPokemonByName($value): ?Pokemon
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.nom = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    public function findPokemonByType($type): array
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
        SELECT P.nom, T.label
        FROM Pokemon P
            INNER JOIN pokemon_type PT ON P.id = PT.pokemon_id
            INNER JOIN type T ON PT.type_id = T.id
        WHERE T.label = :typeName
            ';
        $stmt = $conn->prepare($sql);
        $resultSet = $stmt->executeQuery(['typeName' => $type]);
        return $resultSet->fetchAllAssociative();
    }

    public function findPokemonByPage($page): array
    {
        $conn = $this->getEntityManager()->getConnection();
        $offset = ($page - 1) * 10;
        $sql = '
        SELECT P.nom
        FROM Pokemon P
        LIMIT '.$offset.',10';
        $stmt = $conn->prepare($sql);
        $resultSet = $stmt->executeQuery();
        return $resultSet->fetchAllAssociative();
    }
}
