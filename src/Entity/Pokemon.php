<?php

namespace App\Entity;

use App\Repository\PokemonRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: PokemonRepository::class)]
class Pokemon
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups('pokemons:read')]
    private $id;

    #[ORM\Column(type: 'string', length: 100)]
    #[Groups('pokemons:read')]
    private $nom;

    #[ORM\Column(type: 'text', nullable: true)]
    #[Groups('pokemons:read')]
    private $description;

    #[ORM\ManyToMany(targetEntity: Type::class, inversedBy: 'pokemon')]
    #[Groups('pokemons:read')]
    private $type_pokemon;

    public function __construct()
    {
        $this->type_pokemon = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection<int, Type>
     */
    public function getTypePokemon(): Collection
    {
        return $this->type_pokemon;
    }

    public function addTypePokemon(Type $typePokemon): self
    {
        if (!$this->type_pokemon->contains($typePokemon)) {
            $this->type_pokemon[] = $typePokemon;
        }

        return $this;
    }

    public function removeTypePokemon(Type $typePokemon): self
    {
        $this->type_pokemon->removeElement($typePokemon);

        return $this;
    }
}
