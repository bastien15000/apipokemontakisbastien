<?php

namespace App\Controller;

use App\Repository\PokemonRepository;
use App\Repository\TypeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ApiPokemonController extends AbstractController
{
    // Afficher les 10 premiers pokémons (par défaut)
    #[Route('/api/pokemon', name: 'api_pokemon_index', methods: 'GET')]
    public function index(PokemonRepository $pokemonRepository): Response
    {
        return $this->json($pokemonRepository->findBy(array(),array(),10,0), 200, [], ['groups' => 'pokemons:read']);
    }

    // Afficher tous les pokémons
    #[Route('/api/pokemon/all', name: 'api_pokemon_index', methods: 'GET')]
    public function index_all(PokemonRepository $pokemonRepository): Response
    {
        return $this->json($pokemonRepository->findAll(), 200, [], ['groups' => 'pokemons:read']);
    }

    // Afficher un pokemon en fonction de son nom
    #[Route('/api/pokemon/{nom}', name: 'api_pokemon_by_id', methods: 'GET')]
    public function getPokemonById(PokemonRepository $pokemonRepository, Request $request): Response
    {
        return $this->json($pokemonRepository->findPokemonByName($request->attributes->get('nom')), 200, [], ['groups' => 'pokemons:read']);
    }

    // Afficher les pokemons en fonction de leur type
    #[Route('/api/pokemonType/{type}', name: 'api_pokemon_by_type', methods: 'GET')]
    public function getPokemonByType(PokemonRepository $pokemonRepository, Request $request): Response
    {
        return $this->json($pokemonRepository->findPokemonByType($request->attributes->get('type')), 200, [], ['groups' => 'pokemons:read']);
    }

    // Afficher le nb de pokemons qu'on veut
    #[Route('/api/pokemonNb/{nb}', name: 'api_pokemon_first', methods: 'GET')]
    public function pokemonCount(PokemonRepository $pokemonRepository, Request $request): Response
    {
        return $this->json($pokemonRepository->findBy(array(),array(),$request->attributes->get('nb'),0), 200, [], ['groups' => 'pokemons:read']);
    }

    // Pagination
    #[Route('/api/pokemonPage/{page}', name: 'api_pokemon_first', methods: 'GET')]
    public function pokemonPage(PokemonRepository $pokemonRepository, Request $request): Response
    {
        return $this->json($pokemonRepository->findPokemonByPage($request->attributes->get('page')), 200, [], ['groups' => 'pokemons:read']);
    }
}
