<?php

namespace App\DataFixtures;

use App\Entity\Pokemon;
use App\Entity\Type;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $typeFeu = new Type();
        $typeFeu->setLabel('Feu');

        $typeDragon = new Type();
        $typeDragon->setLabel('Dragon');

        $typeEau = new Type();
        $typeEau->setLabel('Eau');

        //pokemons
        for($i = 0; $i < 10; $i++) {
            $dracaufeu = new Pokemon();
            $dracaufeu->setNom('Dracaufeu'.$i);
            $dracaufeu->setDescription("Lorem ipsum dolor sit, amet consectetur adipisicing elit. Ab, sequi!");
            $dracaufeu->addTypePokemon($typeFeu);
            $dracaufeu->addTypePokemon($typeDragon);
    
            $galopa = new Pokemon();
            $galopa->setNom('Galopa'.$i);
            $galopa->setDescription("Lorem ipsum dolor sit, amet consectetur adipisicing elit. Ab, sequi!");
            $galopa->addTypePokemon($typeFeu);
    
            $tortank = new Pokemon();
            $tortank->setNom('Tortank'.$i);
            $tortank->setDescription("Lorem ipsum dolor sit, amet consectetur adipisicing elit. Ab, sequi!");
            $tortank->addTypePokemon($typeEau);
            
            $manager->persist($dracaufeu);
            $manager->persist($galopa);
            $manager->persist($tortank);
        }

        $manager->persist($typeFeu);
        $manager->persist($typeDragon);
        $manager->persist($typeEau);


        $manager->flush();
    }
}
